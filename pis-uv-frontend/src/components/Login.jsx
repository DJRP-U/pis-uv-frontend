import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';


function Login() {
  return (
    <section className="vh-100 gradient-custom">
      <div className="container py-5 h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-12 col-md-8 col-lg-6 col-xl-5">
            <div className="card bg-dark text-white" style={{ borderRadius: '1rem' }}>
              {/* Esto es un div vacío, si necesita contenido, puede agregarlo */}
              <div style={{ color: 'red' }}></div>

              <div className="card-body p-5 text-center">
                <div className="mb-md-5 mt-md-4 pb-5">
                  <h2 className="fw-bold mb-2 text-uppercase" style={{ color: '#3498db' }}>Login</h2>
                  <p className="text-white-50 mb-5">Semaforo de radiacion UV</p>

                  <div className="form-outline form-white mb-4">
                    <label htmlFor="typeEmailX" style={{ color: '#9b9b9b', fontSize: '' }}>Correo electrónico:</label>
                    <input type="text" id="typeEmailX" className="form-control form-control-lg" style={{ backgroundColor: '#cdcdcd', fontSize: 'smaller' }} placeholder="Por favor ingrese su correo electrónico" />
                  </div>


                  <div className="form-outline form-white mb-4">
                    <label htmlFor="typePasswordX" style={{ color: '#9b9b9b', fontSize: '' }}>Contraseña:</label>
                    <input type="password" id="typePasswordX" className="form-control form-control-lg" style={{ backgroundColor: '#cdcdcd', fontSize: 'smaller' }} placeholder="Por favor ingrese su contraseña" />
                  </div>

                  <div className="row">
                    <div className="row justify-content-center">
                      <div className="col-8 mb-4">
                        <button className="btn btn-primary btn-lg w-100" type="submit">Ingresar</button>
                      </div>
                    </div>
                    <div className="row justify-content-center">
                      <div className="col-8">
                        
                          <button className="btn btn-secondary btn-lg w-100" type="button">
                            Registrar
                          </button>
                        
                      </div>
                    </div>

                  </div>


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Login;
